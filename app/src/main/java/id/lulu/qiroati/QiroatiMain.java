package id.lulu.qiroati;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

public class QiroatiMain extends Activity {

	ImageView back;
	ListView lvqiroati;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qiroatimain);

		back = (ImageView) findViewById(R.id.imgQiroatiback);

		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		String listQiroati[] = new String[] { "Qiroati I", "Qiroati II",
				"Qiroati III", "Qiroati IV", "Qiroati V", "Qiroati VI" };
		lvqiroati = (ListView) findViewById(R.id.lvQiroati);
		AdapterListQiroati adapter = new AdapterListQiroati(
				getApplicationContext(), listQiroati);
		lvqiroati.setAdapter(adapter);
		lvqiroati.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				switch (position) {
				case 0:
					Intent qiraoti1 = new Intent(getApplicationContext(),
							AdvancedListViewActivity.class);
					startActivity(qiraoti1);
					break;

				case 1:
					Intent qiroati2 = new Intent(getApplicationContext(),
							Qiroati2.class);
					startActivity(qiroati2);
					break;

				case 2:
					Intent qiraoti3 = new Intent(getApplicationContext(),
							Qiroati3.class);
					startActivity(qiraoti3);
					break;

					case 3:
						Intent qiraoti4 = new Intent(getApplicationContext(),
								Qiroati4.class);
						startActivity(qiraoti4);
						break;

					case 4:
						Intent qiraoti5 = new Intent(getApplicationContext(),
								Qiroati5.class);
						startActivity(qiraoti5);
						break;

					case 5:
						Intent qiraoti6 = new Intent(getApplicationContext(),
								Qiroati6.class);
						startActivity(qiraoti6);
						break;

				default:
					break;
				}
			}

		});
	}
}
