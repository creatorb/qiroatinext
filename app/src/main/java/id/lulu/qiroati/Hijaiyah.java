package id.lulu.qiroati;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Hijaiyah extends Activity implements android.view.View.OnClickListener{
	
	  private Huruf[] huruf;
	  private int currentHuruf = 0;

	  private MediaPlayer player = new MediaPlayer();
	  private AssetFileDescriptor assetFile;

	  private TextView hurufName;

	  private HurufLoader hurufLoader;
	
	ImageView back,nextImage,prevImage,Suara,hurufView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hijaiyah);
		
		hurufLoader = new HurufLoader(this);
		
		hurufName = (TextView)findViewById(R.id.h_nama);
		Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/black_jack.ttf");
		hurufName.setTypeface(tf);
		
		hurufView = (ImageView)findViewById(R.id.h_gambar);
		hurufView.setOnClickListener(this);
		Suara = (ImageView)findViewById(R.id.play);
		Suara.setOnClickListener(this);
		
		nextImage = (ImageView) this.findViewById( R.id.next );
		nextImage.setOnClickListener(this);
	    prevImage = (ImageView) this.findViewById( R.id.prev );
	    prevImage.setOnClickListener(this);
	    
	    loadHurufIntoArray();

	    updateHurufNama();
	    updateHurufImage();
	    updateHurufSound();
		
		back = (ImageView) findViewById(R.id.imgQiroatiback);

		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}
	
	@Override
	  public void onClick( View v ) {
	    if ( v.getId() == prevImage.getId() )
	      loadPrev();
	    else if ( v.getId() == nextImage.getId() )
	      loadNext();
	    else if ( v.getId() == Suara.getId() )
	      player.start();
	  }
	
	private void updateHurufNama() {
		hurufName.setText( huruf[currentHuruf].getNama() );
	  }

	  private void updateHurufImage() {
	    try {
	      hurufView.setImageBitmap( BitmapFactory.decodeStream(
	          this.getAssets().open(
	              "images/" + huruf[currentHuruf].getImagefile() ) ) );
	    } catch ( IOException e ) {
	      e.printStackTrace();
	    }
	  }

	  private void updateHurufSound() {
	    try {
	      player.reset();
	      assetFile = getAssets().openFd( "sounds/" + huruf[currentHuruf].getSoundfile() );
	      player.setDataSource( assetFile.getFileDescriptor(), assetFile.getStartOffset(), assetFile.getLength() );
	      assetFile.close();
	      player.prepare();
	    } catch ( IOException e ) {
	      e.printStackTrace();
	    }
	  }

	  public void loadNext() {
	    currentHuruf = (currentHuruf + 1) % huruf.length;
	    updateHurufNama();
	    updateHurufImage();
	    updateHurufSound();
	  }

	  public void loadPrev() {
	    currentHuruf = (currentHuruf + huruf.length - 1) % huruf.length;
	    updateHurufNama();
	    updateHurufImage();
	    updateHurufSound();
	  }

	  public void loadHurufIntoArray() {
	    huruf = hurufLoader.getHurufArray();
	  }
}
