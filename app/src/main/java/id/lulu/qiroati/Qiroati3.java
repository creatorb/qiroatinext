package id.lulu.qiroati;

/**
 * Created by b on 10/15/15.
 */

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Qiroati3 extends Activity {

    // variable declaration
    private ListView mainList;
    private MediaPlayer mp;
    private final int[] resID = {R.raw.q3_1, R.raw.q3_2, R.raw.q3_3, R.raw.q3_4, R.raw.q3_5};

    //private int resID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_primer);
// Initializing variables
        mp = new MediaPlayer();

        final Context ctx = getApplicationContext();
        Resources res = ctx.getResources();

        final String[] options = res.getStringArray(R.array.tartil_names_3);
        TypedArray icons = res.obtainTypedArray(R.array.tartil_images_3);


        mainList = (ListView) findViewById(R.id.listView1);
        ArrayAdapter adapter = (new ImageAndTextAdapter(ctx, R.layout.main_list_item, options, icons));

//        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listContent);
        mainList.setAdapter(adapter);

        mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int position, long id) {

                playSong(position);


            }
        });

    }

    public void playSong(int songIndex) {
// Play song
        mp.reset();// stops any current playing song

        mp = MediaPlayer.create(getApplicationContext(), resID[songIndex]);// create's
// new
// mediaplayer
// with
// song.

        mp.start(); // starting mediaplayer

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mp.release();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//// Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main_primer, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//// Handle action bar item clicks here. The action bar will
//// automatically handle clicks on the Home/Up button, so long
//// as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
