package id.lulu.qiroati;

import android.content.Context;

public class HurufLoader {
  private Huruf[] hurufs;

  public HurufLoader( Context context ) {
    loadHurufsFromResources( context );
  }

  public Huruf[] getHurufArray() {
    return this.hurufs;
  }

  private void loadHurufsFromResources( Context context ) {
    String[] hurufNames = context.getResources().getStringArray( R.array.hijaiyah_names );
    String[] hurufImages = context.getResources().getStringArray( R.array.hijaiyah_images );
    String[] hurufSounds = context.getResources().getStringArray( R.array.hijaiyah_sounds );

    hurufs = new Huruf[hurufNames.length];

    for ( int hurufIndex = 0; hurufIndex < hurufNames.length; hurufIndex++ ) {
      Huruf huruf = new Huruf();

      huruf.setName( hurufNames[hurufIndex] );
      huruf.setImagefile( hurufImages[hurufIndex] );
      huruf.setSoundfile( hurufSounds[hurufIndex] );

      hurufs[hurufIndex] = huruf;
    }
  }
}