package id.lulu.qiroati;

public class TartilAdapter {
  private int id;
  private String qiroati_bank_name;
  private String qiroati_bank_image;
  private String qiroati_soundfile;

  public int getId() {
    return id;
  }

  public void setId( int id ) {
    this.id = id;
  }

  public String getQiroati_bank_name() {
    return qiroati_bank_name;
  }

  public void setQiroati_bank_name( String qiroati_bank_name ) {
    this.qiroati_bank_name = qiroati_bank_name;
  }

  public String getQiroati_bank_image() {
    return qiroati_bank_image;
  }

  public void setQiroati_bank_image( String qiroati_bank_image ) {
    this.qiroati_bank_image = qiroati_bank_image;
  }

  public String getQiroati_soundfile() {
    return qiroati_soundfile;
  }

  public void setQiroati_soundfile( String qiroati_soundfile ) {
    this.qiroati_soundfile = qiroati_soundfile;
  }
}