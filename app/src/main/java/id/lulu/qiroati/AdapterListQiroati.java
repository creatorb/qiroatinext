package id.lulu.qiroati;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterListQiroati extends BaseAdapter {

	String[] listqiroati;
	private LayoutInflater inflanter;
	
	public AdapterListQiroati(Context context, String[] listQiroati) {
		inflanter = LayoutInflater.from(context);
		this.listqiroati = listQiroati;
		
	}

	public class ViewHolder{
		TextView text;
	}
	
	@Override
	public int getCount() {
		return listqiroati.length;
	}

	@Override
	public long getItemId(int position) {
		position = listqiroati.length;
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		
		if (view == null) {
			view = inflanter.inflate(R.layout.rowlist_qiroati, null);
			
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.text = (TextView) view.findViewById(R.id.txTitileRwlist);
			view.setTag(viewHolder);
		}

		// fill data
		ViewHolder holder = (ViewHolder) view.getTag();
		String s = listqiroati[position];
		holder.text.setText(s);
		return view;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

}
