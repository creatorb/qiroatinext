package id.lulu.qiroati;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

	LinearLayout LLqiroati,LLhijaiyah,LLlatihan,LLTentang;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		LLqiroati = (LinearLayout)findViewById(R.id.LLqiroati);
		LLqiroati.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), QiroatiMain.class);
				startActivity(i);
			}
		});
		
		LLhijaiyah = (LinearLayout)findViewById(R.id.LLhijaiyah);
		LLhijaiyah.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), Hijaiyah.class);
				startActivity(i);
			}
		});
		
		LLlatihan = (LinearLayout)findViewById(R.id.LLlatihan);
		LLlatihan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), quiz.QuizFunActivity.class);
				startActivity(i);
			}
		});

		LLhijaiyah = (LinearLayout)findViewById(R.id.LLhijaiyah);
		LLhijaiyah.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), Hijaiyah.class);
				startActivity(i);
			}
		});

		LLTentang = (LinearLayout)findViewById(R.id.LLTentang);
		LLTentang.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), Tentang.class);
				startActivity(i);
			}
		});
	}
}
