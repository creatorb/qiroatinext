package id.lulu.qiroati;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

public class SplashScreen extends Activity {

	private final Handler mHandler = new Handler();
	private static final int duration = 5 * 1000;	

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_screen);

		mHandler.postDelayed(mPendingLauncherRunnable,
				SplashScreen.duration);
		
	}

	@Override
	protected void onPause() {
		super.onPause();
		mHandler.removeCallbacks(mPendingLauncherRunnable);
	}

	private final Runnable mPendingLauncherRunnable = new Runnable() {

		public void run() {
			final Intent intent = new Intent(SplashScreen.this,
					MainActivity.class);
			
			startActivity(intent);
			finish();
		}
	};
	
	
	
}
